/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
 що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/ 



/*
1. В чому відмінність між setInterval та setTimeout?
setInterval виконує функцію регулярно, через заданий інтервал, 
тоді як setTimeout виконує функцію лише один раз після вказаного інтервалу.

2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
Для припинення виконання функції, запланованої з використанням setTimeout, використовуйте clearTimeout, 
а з setInterval - clearInterval.
*/



const buttonElement = document.querySelector(".btn");
const pElement = document.querySelector(".paragraph");

buttonElement.addEventListener("click", (event) => {
    setTimeout(() => {
        pElement.innerText = "Операція виконана успішно!"
    }, 3000);
})



const timer = document.querySelector(".timer");

let counter = 10;

const timerElement = setInterval(() => {
    timer.textContent = counter;
    counter--;

    if (counter < 0) {
        timer.innerText = "Зворотній відлік завершено"
        clearInterval(timerElement);
    }

}, 1000)
